export const userName = 'Oleksii Shevchenko';
export const BUTTONS_TEXT = {
	SEARCH: 'Search',
	ADD_COURSE: 'Add course',
	SHOW_COURSE: 'Show course',
	LOG_OUT: 'Log out',
	CREATE_COURSE: 'Create course',
	CREATE_AUTHOR: 'Create author',
	ADD_AUTHOR: 'Add author',
	DELETE_AUTHOR: 'Delete author',
	REGISTRATION_LINK: `Don't have an account?`,
	REGISTRATION_BUTTON: 'sing in',
	LOGIN_LINK: 'Already have an account?',
	LOGIN_BUTTON: 'log in',
};

export const LABEL_TEXT = {
	TITLE: 'Title',
	DESCRIPTION: 'Description',
	AUTHOR_NAME: 'Author name',
	DURATION: 'Duration',
	SEARCH_VALUE: 'Search value',
	EMAIL: 'email',
	PASSWORD: 'password',
	NAME: 'name',
};

export const url = 'http://localhost:4000';

export const storageName = 'userData';
