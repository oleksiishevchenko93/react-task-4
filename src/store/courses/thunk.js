import {
	fetchedCourses,
	fetchedDeleteCourse,
	fetchedUpdateCourse,
	postNewCourse,
} from '../../services';
import {
	addNewCourse,
	deleteCourse,
	setCourses,
	updateCourse,
} from './actionCreators';
import {
	changeMessageSeverity,
	changeMessageText,
	showMessage,
} from '../messages/actionCreators';
import { asyncHideMessage } from '../messages/thunk';

export const deleteCourseThunk = (courseId, token) => {
	return (dispatch) => {
		fetchedDeleteCourse(courseId, token)
			.then(() => {
				dispatch(deleteCourse(courseId));
				dispatch(
					changeMessageText('The course has been removed successfully!')
				);
				dispatch(changeMessageSeverity('success'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};

export const updateCourseThunk = (updatedCourse, token) => {
	return (dispatch) => {
		fetchedUpdateCourse(updatedCourse, token)
			.then(() => {
				dispatch(updateCourse(updatedCourse));
				dispatch(
					changeMessageText('The course has been updated successfully!')
				);
				dispatch(changeMessageSeverity('success'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};

export const getCoursesThunk = (token) => {
	return (dispatch) => {
		fetchedCourses(token)
			.then((courses) => {
				dispatch(setCourses(courses));
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};

export const addCourseThunk = (course, token) => {
	return (dispatch) => {
		postNewCourse(course, token)
			.then(() => {
				dispatch(addNewCourse(course));
				dispatch(changeMessageText('New course has been added successfully!'));
				dispatch(changeMessageSeverity('success'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};
