import {
	ADD_COURSE,
	DELETE_COURSE,
	GET_COURSES,
	UPDATE_COURSE,
} from './actionTypes';

const coursesInitialState = [];

export const coursesReducer = (state = coursesInitialState, action) => {
	switch (action.type) {
		case ADD_COURSE:
			return [...state, action.payload];
		case GET_COURSES:
			return action.payload;
		case DELETE_COURSE:
			return state.filter((course) => course.id !== action.payload);
		case UPDATE_COURSE:
			const index = state.findIndex(
				(course) => course.id === action.payload.id
			);
			const result = [...state];
			result.splice(index, 1, action.payload);
			return result;
		default:
			return state;
	}
};
