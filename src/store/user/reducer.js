import { LOGIN, LOGOUT, SET_USER_PROFILE } from './actionTypes';

const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export const userReducer = (state = userInitialState, action) => {
	switch (action.type) {
		case LOGIN:
			return {
				isAuth: true,
				...action.payload,
			};
		case LOGOUT:
			return {
				isAuth: false,
				name: '',
				email: '',
				token: '',
				role: '',
			};
		case SET_USER_PROFILE:
			return {
				...state,
				...action.payload,
			};
		default:
			return state;
	}
};
