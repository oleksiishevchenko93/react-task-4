import { LOGIN, LOGOUT, SET_USER_PROFILE } from './actionTypes';

export const userLogIn = (form) => {
	return {
		type: LOGIN,
		payload: form,
	};
};

export const userLogOut = () => {
	return {
		type: LOGOUT,
	};
};

export const setUserProfile = (userProfile) => {
	return {
		type: SET_USER_PROFILE,
		payload: userProfile,
	};
};
