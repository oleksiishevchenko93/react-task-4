import { ADD_AUTHOR, GET_AUTHORS } from './actionTypes';

export const setAuthors = (authors) => {
	return {
		type: GET_AUTHORS,
		payload: authors,
	};
};

export const addAuthorToCourse = (author) => {
	return {
		type: ADD_AUTHOR,
		payload: author,
	};
};
