import {
	SET_MESSAGE_SEVERITY,
	SET_MESSAGE_TEXT,
	SHOW_MESSAGE,
	HIDE_MESSAGE,
} from './actionTypes';

const messageInitialState = {
	text: 'Hello!',
	show: false,
	severity: 'info',
};

export const messageReducer = (state = messageInitialState, action) => {
	switch (action.type) {
		case SET_MESSAGE_TEXT:
			return {
				...state,
				text: action.payload,
			};
		case SET_MESSAGE_SEVERITY:
			return {
				...state,
				severity: action.payload,
			};
		case SHOW_MESSAGE:
			return {
				...state,
				show: true,
			};
		case HIDE_MESSAGE:
			return {
				...state,
				show: false,
			};
		default:
			return state;
	}
};
