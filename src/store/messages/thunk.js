import { hideMessage } from './actionCreators';

export const asyncHideMessage = () => {
	const TIMER = 6000;
	return (dispatch) => {
		setTimeout(() => dispatch(hideMessage()), TIMER);
	};
};
