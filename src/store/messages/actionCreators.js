import {
	SET_MESSAGE_SEVERITY,
	SET_MESSAGE_TEXT,
	SHOW_MESSAGE,
	HIDE_MESSAGE,
} from './actionTypes';

export const changeMessageText = (text) => {
	return {
		type: SET_MESSAGE_TEXT,
		payload: text,
	};
};

export const changeMessageSeverity = (severity) => {
	return {
		type: SET_MESSAGE_SEVERITY,
		payload: severity,
	};
};

export const showMessage = () => {
	return {
		type: SHOW_MESSAGE,
	};
};

export const hideMessage = () => {
	return {
		type: HIDE_MESSAGE,
	};
};
