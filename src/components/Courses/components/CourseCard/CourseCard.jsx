import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { getDuration } from '../../../../helpers/pipeDuration';
import { changeDate } from '../../../../helpers/dateGenerator';
import { getLimitAuthors } from '../../../../helpers/authorsGetter';
import { BUTTONS_TEXT } from '../../../../constants';
import Button from '../../../../common/Button/Button';
import { deleteCourseThunk } from '../../../../store/courses/thunk';
import { getUser } from '../../../../store/user/selectors';

import styles from './CourseCard.module.css';

const CourseCard = ({ course, authorsList }) => {
	const { title, description, creationDate, duration, authors } = course;
	const { SHOW_COURSE } = BUTTONS_TEXT;
	const dispatch = useDispatch();
	const userData = useSelector(getUser);
	const condition = userData.role && userData.role === 'admin';
	const navigate = useNavigate();

	const deleteCourseById = (courseId) => {
		dispatch(deleteCourseThunk(courseId, userData.token));
	};

	return (
		<div className={styles.courseCard}>
			<div className={styles.courseCard__content}>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className={styles.courseCard__info}>
				<p>
					<strong>Authors: </strong>
					{getLimitAuthors(authors, authorsList)}
				</p>
				<p>
					<strong>Duration: </strong>
					{getDuration(duration)} hours
				</p>
				<p>
					<strong>Created: </strong>
					{changeDate(creationDate)}
				</p>
				<Button
					buttonText={SHOW_COURSE}
					className={'blue'}
					callback={() => navigate(`/courses/${course.id}`)}
				/>
				{condition && (
					<Button
						buttonText={'update course'}
						className={'green'}
						callback={() => navigate(`/courses/update/${course.id}`)}
					/>
				)}
				{condition && (
					<Button
						buttonText={'Delete'}
						className={'red'}
						callback={() => deleteCourseById(course.id)}
					/>
				)}
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.object.isRequired,
	authorsList: PropTypes.array.isRequired,
};

export default CourseCard;
