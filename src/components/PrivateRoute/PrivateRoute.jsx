import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getUser } from '../../store/user/selectors';

const PrivateRoute = ({ children }) => {
	const navigate = useNavigate();
	const userData = useSelector(getUser);
	const condition = userData.role && userData.role === 'admin';

	useEffect(() => {
		if (!condition) {
			navigate('/courses');
		}
	}, [userData.role]);

	if (condition) {
		return <>{children}</>;
	}
	return <></>;
};

export default PrivateRoute;
