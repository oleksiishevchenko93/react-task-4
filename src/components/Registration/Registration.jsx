import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useHttp } from '../../hooks/HttpHook';
import Form from '../../common/Form/Form';
import { url, BUTTONS_TEXT } from '../../constants';
import {
	changeMessageSeverity,
	changeMessageText,
	showMessage,
} from '../../store/messages/actionCreators';
import { asyncHideMessage } from '../../store/messages/thunk';

const Registration = () => {
	const { request } = useHttp();
	const navigate = useNavigate();
	const [form, setForm] = useState({
		email: '',
		password: '',
		name: '',
	});
	const { REGISTRATION_BUTTON, REGISTRATION_LINK } = BUTTONS_TEXT;
	const dispatch = useDispatch();

	const registerHandler = async (event) => {
		event.preventDefault();
		try {
			const data = await request(`${url}/register`, 'POST', { ...form });
			dispatch(changeMessageText(data.result || 'Success!'));
			dispatch(changeMessageSeverity('success'));
			dispatch(showMessage());
			dispatch(asyncHideMessage());
			navigate('/login');
		} catch (e) {
			dispatch(changeMessageText(e.message));
			dispatch(changeMessageSeverity('error'));
			dispatch(showMessage());
			dispatch(asyncHideMessage());
		}
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value,
		});
	};

	return (
		<div className={'login'}>
			<Form
				linkText={REGISTRATION_LINK}
				onChange={handleChange}
				buttonText={REGISTRATION_BUTTON}
				onSubmit={registerHandler}
				linkPath={'/login'}
				form={form}
				registration={true}
			/>
		</div>
	);
};

export default Registration;
