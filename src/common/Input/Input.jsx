import React from 'react';
import PropTypes from 'prop-types';

import styles from './Input.module.css';

const Input = ({ labelText, placeholderText, onChange, value, type, name }) => {
	return (
		<div className={styles.inputField}>
			<label htmlFor={labelText}>{labelText}</label>
			<input
				name={name}
				className={styles.input}
				id={labelText}
				type={type || 'text'}
				min={0}
				placeholder={placeholderText}
				onChange={onChange}
				value={value}
			/>
		</div>
	);
};

Input.propTypes = {
	labelText: PropTypes.string,
	placeholderText: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
};

export default Input;
