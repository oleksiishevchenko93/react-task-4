import { url } from './constants';

const request = async (url, method = 'GET', body = null, headers = {}) => {
	try {
		if (body) {
			body = JSON.stringify(body);
			headers['Content-Type'] = 'application/json';
		}

		const response = await fetch(url, {
			method,
			body,
			headers,
		});

		const data = await response.json();

		if (!response.ok) {
			throw new Error(
				data.errors || data.result || data.message || 'Something went wrong...'
			);
		}

		return data;
	} catch (e) {
		throw e;
	}
};

export const fetchedCourses = async (token) => {
	try {
		const data = await request(`${url}/courses/all`, 'GET', null, {
			Authorization: token,
		});
		return data.result;
	} catch (e) {
		throw e;
	}
};

export const fetchedAuthors = async (token) => {
	try {
		const data = await request(`${url}/authors/all`, 'GET', null, {
			Authorization: token,
		});
		return data.result;
	} catch (e) {
		throw e;
	}
};

export const logIn = async (form) => {
	try {
		return await request(`${url}/login`, 'POST', { ...form });
	} catch (e) {
		throw e;
	}
};

export const postNewCourse = async (newCourse, token) => {
	try {
		return await request(
			`${url}/courses/add`,
			'POST',
			{ ...newCourse },
			{ Authorization: token }
		);
	} catch (e) {
		throw e;
	}
};

export const fetchedLogoutUser = async (token) => {
	try {
		const response = await fetch(`${url}/logout`, {
			method: 'DELETE',
			body: null,
			headers: { Authorization: token },
		});

		if (!response.ok) {
			throw new Error('Something went wrong...');
		}
	} catch (e) {
		throw e;
	}
};

export const fetchedUserProfile = async (token) => {
	try {
		return await request(`${url}/users/me`, 'GET', null, {
			Authorization: token,
		});
	} catch (e) {
		throw e;
	}
};

export const fetchedDeleteCourse = async (courseId, token) => {
	try {
		return await request(`${url}/courses/${courseId}`, 'DELETE', null, {
			Authorization: token,
		});
	} catch (e) {
		throw e;
	}
};

export const fetchedUpdateCourse = async (updatedCourse, token) => {
	try {
		await request(
			`${url}/courses/${updatedCourse.id}`,
			'PUT',
			{ ...updatedCourse },
			{ Authorization: token }
		);
	} catch (e) {
		throw e;
	}
};

export const fetchedAddAuthor = async (author, token) => {
	try {
		await request(
			`${url}/authors/add`,
			'POST',
			{
				...author,
			},
			{
				Authorization: token,
			}
		);
	} catch (e) {
		throw e;
	}
};
